# Description

This library implements a lightweight profiling class based on **clock_gettime()**. The profiler class can be instantiated as many times as required for profiling different independent tasks. Each profiler can be configured with a string name and will automatically update profiling statistics (minimum and maximum measured time, number of profiling runs, average, variance and total time).

The time measurement accuracy depends on the underlying system (see Linux help on **clock_gettime()** and related timers and functions for more information). The class gets the accuracy and stores it internally, although it is not used for profiling. A member function is provided to get this information (represented as a floating point number and measured in active units).

The time is handled as a float or integer number representing milliseconds or microseconds. Time units are configurable (default units are ms). The selection between floating point and integer representations is done at compile time using a definition. This allows compiling the library without floating point operations, which is sometimes desired for low-power embedded devices.

The time measured for the last profiling task is always available, but previous measurements are not stored. If you require a complete sequence of times for subsequent processing, implement the required memory structures (for example, a float array or vector) to save measured times.

The **CLOCK_REALTIME** timer is used by default to get the actual system-wide time between calls. If required, the profiler instance can be set to use a process-wide or thread-wide timer instead, calling one of the overloaded constructors.

The profile class can be configured to use **clock()** instead of **clock_gettime()**. However, this is not recommended, as **clock()** is much less accurate, it gives CPU time instead of system time and thus the reported time will not include the time the processor is busy running other processes (as commanded by the kernel scheduler) different from the task under measurement.

# Instructions

Using the profiler requires the following steps:

1. Declare a **Profiler** variable using one of the available constructors. If required, set the profiler name and the timer coverage (system, process or thread, using the parameters **CLOCK_REALTIME**, **CLOCK_PROCESS_CPUTIME_ID** and **CLOCK_THREAD_CPUTIME_ID**, respectively).
2. If not done in the constructor, you can setup the profiler name with the **SetName()** member function after creating the object instance.
3. If required, you can get the timer accuracy with **GetTimeAccuracy()**. The result will always be a floating point number in milliseconds (even if the library has been configured to carry out measurements using integers).
4. If required, set the desired units using **SetUnitsMs()** to select milliseconds or **SetUnitsUs()** to select microseconds. By default, the instance will work in milliseconds.
5. Call the **Start()** member function to start measuring time where required.
6. Call the **Stop()** member function to stop measuring time and to update the profile statistics (minimum and maximum measured times, total accumulated time and average time).
7. Call the **GetTimeNNN()** member functions to retrieve measured time and statistics. Take into account that statistics are not updated until the **Stop()** function is called. The **Lapse()** function only updates the measured time.

For more complex profiling tasks or when using a profiler instance as a timer, the **Lapse()** and **Reset()** functions can also be called:

1. If required, call the **Lapse()** member function to retrieve the elapsed time without stopping the timer. The current time will be returned by the function, and it will also be stored in the internal time variable: therefore, you can call **GetTime()** to retrieve the time measured in a previous **Lapse()** call. Take into account this call will modify the profiling results (see the additional considerations below).
2. Whenever required, call **Reset()** to restart statistics (total, average, maximum and minimum times). This does not modify the last measured time.

More than one profiler instance can be declared and used, and they will be completely independent.

# Example code

```
#!C++
#include "Profile.hpp"

using namespace std;

int main( int argc, char** argv ) {
    int nc = 0;
	bool quit = false;
	
	// Setup profilers
	Profiler PrfCapture("Capture");
	
	// Loop for getting profiler estatistics
	while (!quit) {
        	// Start profiler
        PrfCapture.Start();
        [...] // Code to be profiled
        // Stop profiler
        PrfCapture.Stop();
        // Show results
        cout << "Test " << nc << ", time: " << PrfCapture.GetTime() << " ms" << endl;
        // Increment test number and finish when required
        nc++;
        if (nc > 20)
            quit = true;
    }
    // Show profiling statistics
    cout << "Test finished:" << endl;
    cout << "  Total time: " << PrfCapture.GetTotTime() << " ms" << endl;
    cout << "  Min time:   " << PrfCapture.GetMinTime() << " ms" << endl;
    cout << "  Max time:   " << PrfCapture.GetMaxTime() << " ms" << endl;
    cout << "  Avg time:   " << PrfCapture.GetAvgTime() << " ms" << endl;
    
    return 0;
}
```

# Additional considerations

Both **Lapse()** and **Stop()** return the time and save it into the internal Time member, which can be queried later using **GetTime()**. These three functions will give a meaningful time value only when a profiling has been started previously with **Start()**. If any of these functions is called before **Start()**, they return -1 to indicate there is no time to show yet.

The profiling time is saved internally when **Lapse()** and **Stop()** are called.

**Start()** and **Stop()** must be called in this order. If **Stop()** is called before starting, but there is some previous profiling time available (because a profile was run with Start-Stop), the function returns -1 to indicate the error.

**Lapse()** must also be called inside a profiling (i.e., between a **Start()** and a **Stop()**). If called outside, the resulting value is -1.

Therefore, the profiler must be used with strict interface rules: use **Lapse()** inside a profiling task to retrieve the current profile time, use **Stop()** to stop profiling and retrieve the profile time and use **GetTime()** after stopping the profiling to get the resulting time. In any other case, the functions will show the error by returning -1.

The only exception is **GetTime()**: you can call this function both inside and outside a profiling task. But take into account that the time returned by this function is the internally stored profiling time, which is only updated when calling **Lapse()** and **Stop()**. **GetTime()** will give you the last updated time, which will be the previous profiling task time if called inside a profiling task without a previous **Lapse()** call. If this happens to be the first profiling task, the result will be -1.

The **GetXXXTime()** functions return 0 if the specific timing statistic is not available yet. This happens if no profile has been run yet (**Start()** followed by **Stop()** calls), or if the specific magnitude requires more than one time sample to be calculated (for example, the variance).

Time units can be selected to be milliseconds or microseconds using the functions **SetUnitsMs()** and **SetUnitsUs()**. The unit conversion happens after calling **clock_gettime()**, and thus it does not add any overhead to the measurement.

Take into account that calling **Lapse()** while profiling some code (i.e., between **Start()** and **Close()**) will result in a measured time that will include the time spent in the **Lapse()** execution.

When a standard profiling is done with **Start()** and **Stop()**, some additional time corresponding to calling both functions is inevitably included in the measurement. Both functions are kept simple to reduce the overhead as much as possible, but there is no internal correction of the resulting time to take it into account. Keep this in mind when measuring fast codes: the overhead could be relevant.
