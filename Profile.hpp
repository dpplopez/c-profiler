// Profile.hpp
// C++ lightweight profiling library
// ------------------------------------------------------------------------------------------------
// Author:       David Perez-Pinar
// Rev.:         1.5
// Date:         14.08.2014
// Copyright:    2014 David Perez-Pinar
// ------------------------------------------------------------------------------------------------
// Copyright information
//               This program is free software: you can redistribute it and/or modify it under the
//               terms of the GNU General Public License as published by the Free Software
//               Foundation, either version 3 of the License, or (at your option) any later version.
//
//               This program is distributed in the hope that it will be useful, but WITHOUT ANY
//               WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
//               PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
//               You should have received a copy of the GNU General Public License along with this
//               program see the file copying.txt). If not, see <http://www.gnu.org/licenses/>.
//
// ------------------------------------------------------------------------------------------------
// Description:  This library implements a lightweight profiling class based on clock_gettime().
//               The profiler class can be instantiated as many times as required for profiling
//               different independent tasks. Each profiler can be configured with a string name
//               and will automatically update profiling statistics (minimum and maximum measured
//               time, number of profiling runs, average and total time).
//
//               The time is represented as a float or int number in milliseconds or microseconds.
//               The time measurement accuracy depends on the underlying system (see Linux help on
//               clock_gettime() and related timers and functions for more information). The class
//               gets the accuracy and stores it internally. A member function is provided to get
//               this information (represented as a floating point number and measured in active
//               units: milliseconds or microseconds).
//
//               Time units can be set to milliseconds or microseconds through the appropriate
//               configuration function. By default, the library uses milliseconds.
//
//               The time measured for the last profiling task is always available, but previous
//               measurements are not stored. If you require a complete sequence of times for
//               subsequent processing, implement the required memory structures (for example,
//               a float array or vector) to save measured times.
//
//               The timer is called with CLOCK_REALTIME by default to get the actual system-wide
//               time between calls. If required, the profiler instance can be set to use a
//               process-wide or thread-wide timer instead.
//
//               The profile class can be configured to use clock() instead of clock_gettime().
//               However, this is not recommended, as clock() is usually less accurate and it
//               gives CPU time instead of system time (and thus it will not include the time spent
//               by the processor on other processes that interrupt the task under measurement).
//               
// ------------------------------------------------------------------------------------------------
// Instructions: Using the profiler requires the following steps:
//
//               1. Declare a Profiler variable using one of the available constructors. If
//                  required, set the profiler name and the timer coverage (system, process or
//                  thread, using the parameters CLOCK_REALTIME, CLOCK_PROCESS_CPUTIME_ID and
//                  CLOCK_THREAD_CPUTIME_ID, respectively).
//               2. If not done in the constructor, you can setup the profiler name with the
//                  SetName() member function after creating the object instance.
//               3. If required, you can get the timer accuracy with GetTimeAccuracy(). The result
//                  will always be a floating point number in milliseconds (even if the library
//                  has been configured to carry out measurements using integers).
//               3. Call the Start() member function to start measuring time where required.
//               4. Call the Lapse() member function to retrieve elapsed time without stopping
//                  the timer where required. The time will be returned by the function, and it
//                  will also be stored in the internal time variable (therefore, you can call
//                  GetTime() to retrieve the time measured in a previous Lapse() call.
//               5. Call the Stop() member function to stop measuring time and to update the
//                  profile statistics (minimum and maximum measured times, total accumulated
//                  time and average time).
//               6. Call the GetXXXTime() member functions to retrieve measured time and
//                  statistics. Take into account that statistics are not updated until the Stop()
//                  function is called. The Lapse() function updates only the measured time.
//               7. Whenever required, call Reset() to restart statistics (total, average, maximum
//                  and minimum times). This does not modify the last measured time.
//
//               Both Lapse() and Stop() return the time and save it into the internal Time member,
//               which can be queried later using GetTime(). These three functions will give a
//               meaningful time value only when a profiling has been started previously with
//               Start(). If any of these functions is called before Start(), they return -1
//               to indicate there is no time to show yet.
//
//               The profiling time is saved internally when Lapse() and Stop() are called.
//
//               Start() and Stop() must be called in this order. If Stop() is called before
//               starting, but there is some previous profiling time available (because a profile
//               was run with Start-Stop), the function returns -1 to indicate the error.
//
//               Lapse() must also be called inside a profiling (i.e., between a Start() and a
//               Stop()). If called outside, the resulting value is -1.
//
//               Therefore, the profiler must be used with strict interface rules: use Lapse()
//               inside a profiling task to retrieve the current profile time, use Stop() to
//               stop profiling and retrieve the profile time and use GetTime() after stopping the
//               profiling to get the resulting time. In any other case, the functions will show
//               the error by returning -1.
//
//               The only exception is GetTime(): you can call this function both inside and
//               outside a profiling task. But take into account that the time returned by this
//               function is the internally stored profiling time, which is only updated when
//               calling Lapse() and Stop(). GetTime() will give you the last updated time, which
//               will be the previous profiling task time if called inside a profiling task without
//               a previous Lapse() call. If this happens to be the first profiling task, the
//               result will be -1.
//
//               The GetXXXTime() functions return 0 if the specific timing statistic is not
//               available yet. This happens if no profile has been run yet (Start() followed by
//               Stop() calls), or if the specific magnitude requires more than one time sample
//               to be calculated (for example, the variance).
//
//               Time units can be selected to be milliseconds or microseconds. The unit conversion
//               happens after calling clock_gettime(), and thus it does not add any overhead to
//               the measurement.
//
// ------------------------------------------------------------------------------------------------
// Change log:   Rev.  Description
//               1.0   Library first implementation using functions and global variables
//               1.1   Library modified to encapsulate profiler in a class
//               1.2   Float or integer modes for time representation and calculations option added
//               1.3   Additional option to allow generating a float average for integer mode
//               1.4   Measured time variance added to statistics (Welford runtime method)
//               1.5   Unavailable time handling modified to reduce overhead
//                     Interface function added to get the number of profiling runs executed
//                     Time units can be selected between milliseconds and microseconds
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __PROFILE_HPP__
#define __PROFILE_HPP__

// -- External libraries --------------------------------------------------------------------------

#include <sstream>
#include <string>
#include <stdio.h>
#include <unistd.h>

using namespace std;

// -- Time representation accuracy ----------------------------------------------------------------
// Allows selecting between floating point and integer accuracy. Both options represent time in
// milliseconds. The timer source can also be forced to clock() (using jiffies, low precision and
// low accuracy).

#define PRF_FLOAT_MS                // Define to use floating point variables for time values
#define PRF_STAFLOAT_MS             // Define to use floats for average time in integer mode
//#define PROFILE_WITH_CLOCK          // Define to force using clock() as time source

// -- POSIX compliance checks ---------------------------------------------------------------------

#ifndef _POSIX_TIMERS
#pragma message "POSIX timers not defined, using clock() for timing"
#define PROFILE_WITH_CLOCK
#ifndef CLOCK_REALTIME
#define CLOCK_REALTIME 0
#endif
#else
#ifndef _POSIX_CPUTIME
#pragma message "POSIX CPU process timer not available"
#endif
#ifndef _POSIX_THREAD_CPUTIME
#pragma message "PSIX CPU thread timer not available"
#endif
#endif

// -- Configuration dependent types ---------------------------------------------------------------

#ifdef PRF_FLOAT_MS
#define PRF_RETTYPE float
#define PRF_STATYPE float
#else
#define PRF_RETTYPE int
#ifdef PRF_STAFLOAT_MS
#define PRF_STATYPE float
#else
#define PRF_STATYPE int
#endif
#endif

#ifdef PROFILE_WITH_CLOCK
#define PRF_TMTYPE clock_t
#else
#define PRF_TMTYPE timespec
#endif

// == Profile class definition ====================================================================

class Profiler {
private:
	// Profiler configuration
	string      Name;               // Configurable profiler name
	clockid_t   TimerType;          // Profiler timer type
	bool        UnitsMs;            // Indicates the currently active units (ms or us)
	// Profiler timer variables
	PRF_TMTYPE  TimeStart;          // Profile interval start time as provided by timer
	PRF_TMTYPE  TimeEnd;            // Profile interval end time as provided by timer
	// Profiler status
	bool        Started;
	// Variance run-time calculation variables
	PRF_STATYPE VarM;
	PRF_STATYPE VarS;
	// Profiler time result variables
	PRF_RETTYPE Time;
	PRF_RETTYPE MaxTime;
	PRF_RETTYPE MinTime;
	PRF_RETTYPE TotTime;
	PRF_STATYPE AvgTime;
	PRF_STATYPE VarTime;
	int         Cnt;
	// Profiler internal functions
	void        Setup(clockid_t type,string name);
	PRF_RETTYPE ProfileDiffMs(PRF_TMTYPE start, PRF_TMTYPE end);
public:
	// Constructors
	Profiler();
	Profiler(string name);
	Profiler(clockid_t type);
	Profiler(clockid_t type, string name);
	// Configuration functions
	void        SetName(string name);    // Set profiler name
	string      GetName();               // Get profiler name
	void        SetUnitsMs();            // Set time units to ms
	void        SetUnitsUs();            // Set time units to us
	float       GetTimeAccuracy();       // Get the system timer accuracy (ms)
	// Profiling functions
	void        Reset();                 // Reset profiler statistics
	void        Start();                 // Start profiling
	PRF_RETTYPE Lapse();                 // Get current elapsed time
	PRF_RETTYPE Stop();                  // Stop profiling and get measured time
	// Results access functions
	PRF_RETTYPE GetTime();               // Get the time measured in the last profile (ms)
	PRF_RETTYPE GetMaxTime();            // Get the maximum measured time (ms)
	PRF_RETTYPE GetMinTime();            // Get the minimum measured time (ms)
	PRF_RETTYPE GetTotTime();            // Get the total (accumulated) measured time (ms)
	PRF_STATYPE GetAvgTime();            // Get the average measured time (ms)
	PRF_STATYPE GetVarTime();            // Get the variance of measured time (ms²)
	int         GetCnt();                // Get the number of profiling runs executed
};

// == Profiler class implementation ===============================================================

// -- Construction and destruction ----------------------------------------------------------------

Profiler::Profiler() {
	Setup(CLOCK_REALTIME,"");
	Reset();
	Started = false;
}

Profiler::Profiler(string name) {
	Setup(CLOCK_REALTIME,name);
	Reset();
	Started = false;
}

Profiler::Profiler(clockid_t type) {
	Setup(type,"");
	Reset();
	Started = false;
}

Profiler::Profiler(clockid_t type,string name) {
	Setup(type,name);
	Reset();
	Started = false;
}

// -- Internal setup functions --------------------------------------------------------------------

void Profiler::Setup(clockid_t type,string name) {
	Name = name;
	UnitsMs = true;
	switch (type) {
	case CLOCK_REALTIME:
#ifdef _POSIX_CPUTIME
	case CLOCK_PROCESS_CPUTIME_ID:
#endif
#ifdef _POSIX_THREAD_CPUTIME
	case CLOCK_THREAD_CPUTIME_ID:
#endif
		TimerType = type;
		break;
	default:
		TimerType = CLOCK_REALTIME;
		break;
	}
}

// -- Internal timing functions -------------------------------------------------------------------

PRF_RETTYPE Profiler::ProfileDiffMs(PRF_TMTYPE start, PRF_TMTYPE end) {
#ifdef PROFILE_WITH_CLOCK
	if (UnitsMs)
		return (PRF_RETTYPE) (1000*(PRF_RETTYPE) (TimeEnd-TimeStart)/(PRF_RETTYPE) CLOCKS_PER_SEC);
	else
		return (PRF_RETTYPE) (1000000*(PRF_RETTYPE) (TimeEnd-TimeStart)/(PRF_RETTYPE) CLOCKS_PER_SEC);
#else
	timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	}
	else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	if (UnitsMs)
		return (PRF_RETTYPE) (1000*(PRF_RETTYPE) temp.tv_sec + ((PRF_RETTYPE) temp.tv_nsec/1000000));
	else
		return (PRF_RETTYPE) (1000000*(PRF_RETTYPE) temp.tv_sec + ((PRF_RETTYPE) temp.tv_nsec/1000));
#endif
}

// -- Profiler interface implementation -----------------------------------------------------------

void Profiler::SetName(string name) {
	Name = name;
}

string Profiler::GetName() {
	return Name;
}

float Profiler::GetTimeAccuracy() {
#ifdef PROFILE_WITH_CLOCK
	return 1.0/(float) sysconf(_SC_CLK_TCK);
#else
	timespec temp;
	clock_getres(TimerType,&temp);
	return 1000.0*(float) temp.tv_sec + 0.000001*(float) temp.tv_nsec;
#endif
}

void Profiler::SetUnitsMs() {
	if (!UnitsMs) {
		UnitsMs = true;
		TotTime = TotTime/1000;
		MaxTime = MaxTime/1000;
		MinTime = MinTime/1000;
		AvgTime = AvgTime/1000;
		VarTime = VarTime/1000000;
	}
}

void Profiler::SetUnitsUs() {
	if (UnitsMs) {
		UnitsMs = false;
		TotTime = 1000*TotTime;
		MaxTime = 1000*MaxTime;
		MinTime = 1000*MinTime;
		AvgTime = 1000*AvgTime;
		VarTime = 1000000*VarTime;
	}
}

void Profiler::Reset() {
	VarM = 0;
	VarS = 0;
	Time = -1;
	TotTime = 0;
	AvgTime = 0;
	VarTime = 0;
	MaxTime = 0;
	MinTime = 0;
	Cnt = 0;
}

void Profiler::Start() {
#ifdef PROFILE_WITH_CLOCK
	TimeStart = clock();
#else
	clock_gettime(TimerType,&TimeStart);
#endif
	Started = true;
}

PRF_RETTYPE Profiler::Lapse() {
	if (!Started)
		return -1;
#ifdef PROFILE_WITH_CLOCK
	TimeEnd = clock();
#else
	clock_gettime(TimerType,&TimeEnd);
#endif
	Time = ProfileDiffMs(TimeStart,TimeEnd);
	return Time;
}

PRF_RETTYPE Profiler::Stop() {
	if (Lapse() < 0)
		return -1;
	Started = false;
	Cnt++;
	if (Cnt == 1) {
		MinTime = Time;
		MaxTime = Time;
		VarM = (PRF_STATYPE) Time;
		VarS = 0;
		VarTime = 0;
	}
	else {
		if (Time < MinTime)
			MinTime = Time;
		if (Time > MaxTime)
			MaxTime = Time;
		PRF_STATYPE Mold = VarM;
		VarM = Mold + ((PRF_STATYPE) Time - Mold)/(PRF_STATYPE) Cnt;
		VarS = VarS + ((PRF_STATYPE) Time - Mold)*((PRF_STATYPE) Time - VarM);
		VarTime = VarS/((PRF_STATYPE) Cnt - 1);
	}
	TotTime += Time;
	AvgTime = (PRF_STATYPE) TotTime/(PRF_STATYPE) Cnt;
	return Time;
}

PRF_RETTYPE Profiler::GetTime() {
	return Time;
}

PRF_RETTYPE Profiler::GetMaxTime() {
	return MaxTime;
}

PRF_RETTYPE Profiler::GetMinTime() {
	return MinTime;
}

PRF_RETTYPE Profiler::GetTotTime() {
	return TotTime;
}

PRF_STATYPE Profiler::GetAvgTime() {
	return AvgTime;
}

PRF_STATYPE Profiler::GetVarTime() {
	return VarTime;
}

int Profiler::GetCnt() {
	return Cnt;
}
#endif //__PROFILE_HPP__
